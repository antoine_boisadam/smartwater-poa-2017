package com.antoine.smartwater.entities;

import android.bluetooth.BluetoothDevice;
import android.os.Parcel;
import android.os.Parcelable;

import com.antoine.smartwater.utils.WifiDevice;

import java.io.Serializable;

/**
 * Created by antoine on 19/05/17.
 */

public class Sensor implements Parcelable, Serializable {

    public enum SensorType {
        TAP,
        SHOWER,
        UNKNOWN;

        public static String[] names() {
            SensorType[] states = values();
            String[] names = new String[states.length];

            for (int i = 0; i < states.length; i++) {
                names[i] = states[i].name();
            }

            return names;
        }
    }

    public enum Mode {
        BLUETOOTH,
        WIFI,
        UNKNOWN
    }

    private String id;
    private String name;
    private String userid;
    private String mRoom; // Bathroom, kitchen, ...
    private String addressMac;
    private Mode mode;
    private SensorType mType;

    private boolean disabled = true;

    public Sensor() {
    }

    public Sensor(String id, String name, String room, String macAddr, Mode mode, SensorType type, String userid) {
        this.id = id;
        this.name = name;
        this.addressMac = macAddr;
        this.mRoom = room;
        this.mode = mode;
        this.mType = type;
        this.userid = userid;
    }

    public Sensor(String id, String name, String room, SensorType type, BluetoothDevice device, String userid) {
        this.id = id;
        this.name = name;
        this.mRoom = room;
        this.mType = type;

        this.addressMac = device.getAddress();
        this.mode = Mode.BLUETOOTH;

        this.userid = userid;
    }

    public Sensor(String id, String name, String room, SensorType type, WifiDevice device, String userid) {
        this.id = id;
        this.name = name;
        this.mRoom = room;
        this.mType = type;

        this.addressMac = device.getMac();
        this.mode = Mode.WIFI;

        this.userid = userid;
    }

    public Sensor(Parcel in) {
        id = in.readString();
        name = in.readString();
        mRoom = in.readString();
        addressMac = in.readString();
        try {
            mode = Mode.valueOf(in.readString());
        } catch (Exception e) {
            mode = Mode.UNKNOWN;
        }
        try {
            mType = SensorType.valueOf(in.readString());
        } catch (Exception e) {
            mType = SensorType.UNKNOWN;
        }
        userid = in.readString();
    }

    public static final Creator<Sensor> CREATOR = new Creator<Sensor>() {
        @Override
        public Sensor createFromParcel(Parcel in) {
            return new Sensor(in);
        }

        @Override
        public Sensor[] newArray(int size) {
            return new Sensor[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(mRoom);
        dest.writeString(addressMac);
        dest.writeString(mode.name());
        dest.writeString(mType.name());
        dest.writeString(userid);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoom() {
        return mRoom;
    }

    public void setRoom(String room) {
        this.mRoom = room;
    }

    public SensorType getType() {
        return mType;
    }

    public void setType(SensorType type) {
        this.mType = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMac() {
        return addressMac;
    }

    public void setMac(String addressMac) {
        this.addressMac = addressMac;
    }

    public Mode getMode() {
        return mode;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public void enable() {
        disabled = false;
    }

    public void disable() {
        disabled = true;
    }

    public boolean isEnabled() {
        return !disabled;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
}

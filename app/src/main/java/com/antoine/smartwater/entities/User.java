package com.antoine.smartwater.entities;

import com.antoine.smartwater.utils.Constants;
import com.google.firebase.auth.FirebaseUser;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by antoine on 23/05/17.
 */

public class User implements Serializable {

    // Attributes
    private String uid;
    private String displayName;
    private String email;
    private Configuration configuration;

    private boolean isAdmin = false;

    private String house;

    // Constructors
    public User() {
    }

    public User(FirebaseUser user) {
        this.uid = user.getUid();
        this.displayName = user.getDisplayName();
        this.email = user.getDisplayName();

        this.configuration = new Configuration();
    }

    public User(String name, FirebaseUser user, boolean isAdmin) {
        this(user);
        this.displayName = name;
        this.isAdmin = isAdmin;
    }

    public User(String id, String displayName, String email, boolean isAdmin, Configuration config, ArrayList<Sensor> sensors, ArrayList<Recording> recordings) {
        this.uid = id;
        this.displayName = displayName;
        this.email = email;
        this.isAdmin = isAdmin;
        this.configuration = config;
    }

    public User(String uid, String displayName, String email, boolean isAdmin) {
        this.uid = uid;
        this.displayName = displayName;
        this.email = email;
        this.isAdmin = isAdmin;

        this.configuration = new Configuration();
    }

    // Getter and setter

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }
}

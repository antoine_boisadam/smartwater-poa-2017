package com.antoine.smartwater.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by antoine on 27/06/17.
 */

public class House implements Serializable {
    private String name;
    private User admin;

    public House() {
        this.admin = new User();
    }

    public House(String name) {
        this();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public House(String name, User admin) {
        this.name = name;
        this.admin = admin;
    }

    public User getAdmin() {
        return admin;
    }

    public void setAdmin(User admin) {
        this.admin = admin;
    }
}

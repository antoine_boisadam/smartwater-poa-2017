package com.antoine.smartwater.entities;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by antoine on 14/06/17.
 */

public class Configuration implements Serializable {
    private int goal;
    private Date date;

    public Configuration() {
        this.goal = 20;
        this.date = new Date(System.currentTimeMillis());
    }

    public Configuration(int goal, long date) {
        this(goal, new Date(date));
    }

    public Configuration(int goal, Date date) {
        this.goal = goal;
        this.date = date;
    }

    public int getGoal() {
        return goal;
    }

    public void setGoal(int goal) {
        this.goal = goal;
    }

    public long getDate() {
        return date.getTime();
    }

    public void setDate(long date) {
        this.date = new Date(date);
    }
}

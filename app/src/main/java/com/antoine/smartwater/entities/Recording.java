package com.antoine.smartwater.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.antoine.smartwater.entities.Sensor.SensorType;

import java.io.Serializable;
import java.util.Date;
import java.util.StringTokenizer;

/**
 * Created by antoine on 19/05/17.
 */

public class Recording implements Parcelable, Serializable {
    private float consumption;
    private Date date;
    private SensorType sensorType;
    private String sensorid;
    private String userid;

    public Recording() {
        this.date = new Date();
        this.sensorType = SensorType.UNKNOWN;
    }

    public Recording(float consumption, Sensor sensor, long date, String userid) {
        this.consumption = consumption;
        this.date = new Date(date);
        this.sensorType = sensor.getType();
        this.sensorid = sensor.getId();
        this.userid = userid;
    }

    public Recording(float consumption, Sensor sensor, Date date, String userid) {
        this.consumption = consumption;
        this.date = date;
        this.sensorType = sensor.getType();
        this.sensorid = sensor.getId();
        this.userid = userid;
    }

    public Recording(Parcel in) {
        consumption = in.readFloat();
        date = new Date(in.readLong());
        try {
            sensorType = SensorType.valueOf(in.readString());
        } catch (Exception e) {
            sensorType = SensorType.UNKNOWN;
        }
        sensorid = in.readString();
        userid = in.readString();
    }

    public static final Creator<Recording> CREATOR = new Creator<Recording>() {
        @Override
        public Recording createFromParcel(Parcel in) {
            return new Recording(in);
        }

        @Override
        public Recording[] newArray(int size) {
            return new Recording[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(consumption);
        dest.writeLong(date.getTime());
        dest.writeString(sensorType.name());
        dest.writeString(sensorid);
        dest.writeString(userid);
    }

    public float getConsumption() {
        return consumption;
    }

    public void setConsumption(float consumption) {
        this.consumption = consumption;
    }

    public String getSensorid() {
        return sensorid;
    }

    public void setSensorid(String sensorid) {
        this.sensorid = sensorid;
    }

    public long getDate() {
        return date.getTime();
    }

    public void setDate(long date) {
        this.date = new Date(date);
    }

    public SensorType getSensorType() {
        return sensorType;
    }

    public void setSensorType(SensorType sensorType) {
        this.sensorType = sensorType;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
}

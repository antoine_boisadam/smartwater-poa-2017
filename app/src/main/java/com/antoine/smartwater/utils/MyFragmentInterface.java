package com.antoine.smartwater.utils;

import android.support.annotation.Nullable;

import com.antoine.smartwater.entities.Recording;
import com.antoine.smartwater.entities.Sensor;
import com.antoine.smartwater.entities.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by antoine on 02/06/17.
 */

public interface MyFragmentInterface {
    void updateFragment(@Nullable User user, @Nullable ArrayList<Sensor> sensors, @Nullable ArrayList<Recording> recordings);
}

package com.antoine.smartwater.utils;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.antoine.smartwater.activities.MainActivity;
import com.antoine.smartwater.entities.Sensor;

import java.lang.ref.WeakReference;

/**
 * Created by antoine on 29/05/17.
 */

public class BluetoothHandler extends Handler {
    private final WeakReference<Activity> mActivity;
    private Sensor mSensor;

    public BluetoothHandler(Activity activity, Sensor sensor) {
        mActivity = new WeakReference<>(activity);
        mSensor = sensor;
    }

    @Override
    public void handleMessage(Message msg) {
        final Activity activity = mActivity.get();

        switch (msg.what) {
            case Constants.MESSAGE_STATE_CHANGE:
                switch (msg.arg1) {
                    case Constants.STATE_CONNECTED:
                        Log.i(Constants.TAG, "Connected");
                        break;
                    case Constants.STATE_CONNECTING:
                        Log.i(Constants.TAG, "Connecting");
                        break;
                    case Constants.STATE_NONE:
                        Log.i(Constants.TAG, "Not connected");
                        break;
                    case Constants.STATE_ERROR:
                        Log.i(Constants.TAG, "Error");
                        break;
                }
                break;
            case Constants.MESSAGE_WRITE:
                byte[] writeBuf = (byte[]) msg.obj;
                // construct a string from the buffer
                String writeMessage = new String(writeBuf);
                // Log it
                Log.i(Constants.TAG, "Writing message: " + writeMessage);
                break;
            case Constants.MESSAGE_READ:
                String readMessage = (String) msg.obj;

                if (readMessage != null) {
                    float val;
                    try {
                        val = Float.valueOf(readMessage);
                    } catch (Exception e) {
                        val = 0;
                    }
                    Log.i(Constants.TAG, "Receive from BT: " + readMessage.trim());
                    try {
                        ((MainActivity) activity).addRecord(val, mSensor);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

            case Constants.MESSAGE_TOAST:
                Toast.makeText(activity.getApplicationContext(), msg.getData().getString(Constants.TOAST), Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
package com.antoine.smartwater.utils;

import com.antoine.smartwater.entities.Recording;
import com.antoine.smartwater.entities.Sensor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by antoine on 04/07/17.
 */

public class Utils {
    // Add empty value for missing days and sum same day values
    public static ArrayList<Recording> getAllRecordingsByDay(ArrayList<Recording> recordings) {
        ArrayList<Recording> daily = new ArrayList<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyy");

        if(recordings.size() == 0) {
            return daily;
        }

        int i = 0; // current formatted index (equivalent to formatted.size()-1)
        daily.add(recordings.get(0));
        // Concat all same days records + append missing days between 2 records
        for(int j = 1; j < recordings.size(); j++) {
            if(dateFormat.format(daily.get(i).getDate()).equals(dateFormat.format(recordings.get(j).getDate()))) {
                // Same day
                Recording r = daily.get(i);
                r.setConsumption(recordings.get(j).getConsumption()+r.getConsumption());
                daily.set(i, r);
            } else {
                // Other day

                // Try to append some day between
                for(long k = (daily.get(i).getDate()/Constants.ONE_DAY)+1; k < recordings.get(j).getDate()/Constants.ONE_DAY; k++) {
                    daily.add(new Recording(0, new Sensor(), k*Constants.ONE_DAY + Constants.ONE_DAY/2, null)); // We add half a day
                    i++;
                }

                // And we add our recording
                daily.add(recordings.get(j));
                i++;
            }
        }

        // Append empty recording from last one to today
        long now = System.currentTimeMillis();
        for(long k = (daily.get(i).getDate()/Constants.ONE_DAY)+1; k < now/Constants.ONE_DAY; k++) {
            daily.add(new Recording(0, new Sensor(), k*Constants.ONE_DAY + Constants.ONE_DAY/2, null)); // We add half a day
        }

        return daily;
    }

    // Return only days with recordings
    public static ArrayList<Recording> getRecordingsByDay(ArrayList<Recording> recordings) {
        ArrayList<Recording> daily = new ArrayList<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyy");

        if(recordings.size() == 0) {
            return daily;
        }

        int i = 0; // current formatted index (equivalent to formatted.size()-1)
        daily.add(recordings.get(0));
        // Concat all same days records + append missing days between 2 records
        for(int j = 1; j < recordings.size(); j++) {
            if(dateFormat.format(daily.get(i).getDate()).equals(dateFormat.format(recordings.get(j).getDate()))) {
                // Same day
                Recording r = daily.get(i);
                r.setConsumption(recordings.get(j).getConsumption()+r.getConsumption());
                daily.set(i, r);
            } else {
                // And we add our recording
                daily.add(recordings.get(j));
                i++;
            }
        }

        return daily;
    }
}

package com.antoine.smartwater.utils;

import java.util.UUID;

/**
 * Created by antoine on 19/05/17.
 */

public interface Constants {

    String TAG = "Smartwater";

    // message types sent from the BluetoothChatService Handler
    int MESSAGE_STATE_CHANGE = 1;
    int MESSAGE_READ = 2;
    int MESSAGE_WRITE = 3;
    int MESSAGE_TOAST = 4;

    // Constants that indicate the current connection state
    int STATE_NONE = 0;       // we're doing nothing
    int STATE_ERROR = 1;
    int STATE_CONNECTING = 2; // now initiating an outgoing connection
    int STATE_CONNECTED = 3;  // now connected to a remote device

    UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"); // "b2157530-3bf0-11e7-9598-0800200c9a66"

    String INTENT_USER = "intent_extra";
    String INTENT_EMAIL = "intent_email";
    String INTENT_PASSWORD = "intent_password";
    String INTENT_ADMIN = "intent_admin";

    String TOAST = "toast";

    String ARG_USER = "arg_user";
    String ARG_SENSORS = "arg_sensors";
    String ARG_RECORDINGS = "arg_recordings";
    String ARG_LAYOUT = "arg_layout";


    int REQUEST_ENABLE_BT = 1;

    long ONE_DAY = 1 * 24 * 60 * 60 * 1000;
}
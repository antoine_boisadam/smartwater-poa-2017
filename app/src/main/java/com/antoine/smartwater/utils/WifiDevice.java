package com.antoine.smartwater.utils;

/**
 * Created by antoine on 01/06/17.
 */

public class WifiDevice {
    private String ip;
    private String mac;

    public WifiDevice(String ip, String mac) {
        this.ip = ip;
        this.mac = mac;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }
}

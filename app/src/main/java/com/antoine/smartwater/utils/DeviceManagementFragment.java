package com.antoine.smartwater.utils;

import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.antoine.smartwater.R;
import com.antoine.smartwater.activities.MainActivity;
import com.antoine.smartwater.activities.SensorsFragment;
import com.antoine.smartwater.entities.Sensor;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by antoine on 06/07/17.
 */

public abstract class DeviceManagementFragment extends Fragment {

    protected BluetoothAdapter mBluetoothAdapter;

    public DeviceManagementFragment() {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    protected abstract void endOfBtnSpin();

    protected void checkForBluetooth() {
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
            new AlertDialog.Builder(getContext())
                    .setTitle(getString(R.string.doesnt_support_bluetooth_title))
                    .setMessage(getString(R.string.doesnt_support_bluetooth_message))
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            endOfBtnSpin();
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                // Bluetooth is not enable :)
                new AlertDialog.Builder(getContext())
                        .setTitle(getString(R.string.bluetooth_disabled_title))
                        .setMessage(getString(R.string.bluetooth_disabled_message))
                        .setPositiveButton(R.string.enable_bluetooth, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                                startActivityForResult(enableBtIntent, Constants.REQUEST_ENABLE_BT);
                                // onActivityResult()
                            }
                        })
                        .setNegativeButton(R.string.not_now, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                endOfBtnSpin();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            } else {
                bluetoothDiscovery();
            }
        }
    }

    protected void bluetoothDiscovery() {
        // Bluetooth should be supported and enabled
        mBluetoothAdapter.startDiscovery();
    }

    protected HashMap<String, WifiDevice> refreshArp() {
        HashMap<String, WifiDevice> wifiDevices = new HashMap<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader("/proc/net/arp"));
            String line;
            while ((line = br.readLine()) != null) {
                Log.d("BufferedReader", line);
                // IP address, HW type, Flags, HW address, Mask, Device
                String[] splitted = line.split(" +");
                String ip = splitted[0];
                String mac = splitted[3];
                if (mac.matches("..:..:..:..:..:..")) { // We don't add the header
                    wifiDevices.put(mac, new WifiDevice(ip, mac));
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(getContext(), "Is your wifi enabled?", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getContext(), "Unknown error, please try again", Toast.LENGTH_SHORT).show();
        }

        return wifiDevices;
    }
}

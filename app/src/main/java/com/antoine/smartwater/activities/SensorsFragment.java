package com.antoine.smartwater.activities;

/**
 * Created by antoine on 16/05/17.
 */


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.antoine.smartwater.R;
import com.antoine.smartwater.adapters.SensorsAdapter;
import com.antoine.smartwater.entities.Recording;
import com.antoine.smartwater.entities.Sensor;
import com.antoine.smartwater.entities.User;
import com.antoine.smartwater.utils.BluetoothService;
import com.antoine.smartwater.utils.Constants;
import com.antoine.smartwater.utils.DeviceManagementFragment;
import com.antoine.smartwater.utils.MyFragmentInterface;
import com.antoine.smartwater.utils.WifiDevice;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;

import static android.app.Activity.RESULT_OK;
import static com.antoine.smartwater.entities.Sensor.*;
import static com.antoine.smartwater.utils.Constants.REQUEST_ENABLE_BT;
import static com.antoine.smartwater.utils.Constants.STATE_CONNECTED;
import static com.antoine.smartwater.utils.Constants.STATE_ERROR;


public class SensorsFragment extends DeviceManagementFragment implements MyFragmentInterface {

    private int mLayoutId;

    private ListView listView;
    private SensorsAdapter mAdapter;
    private ArrayList<Sensor> sensorsList;

    private User mUser;
    private Switch mSwitch;
    private HashMap<String, BluetoothDevice> btDevices;

    private Button addBtn;
    private boolean endOfDiscovery;

    private Button refreshBtn;
    private ProgressBar refreshBtnSpinner;

    private final BroadcastReceiver mBluetoothReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                Log.d(Constants.TAG, "SensorsFragment.onReceive: " + "Discovery started");
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                //discovery finishes
                Log.d(Constants.TAG, "SensorsFragment.onReceive: " + "Discovery finishes");
                refreshBtnSpinner.setVisibility(View.GONE);
                refreshBtn.setVisibility(View.VISIBLE);
                endOfDiscovery = true;
            } else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                Log.d(Constants.TAG, "SensorsFragment.onReceive: " + "Device found");
                //bluetooth device found
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (!btDevices.containsKey(device.getAddress())) {
                    btDevices.put(device.getAddress(), device);
                    for (Sensor s : sensorsList) {
                        Log.d(Constants.TAG, s.getMac());
                        if (btDevices.containsKey(s.getMac())) { // Can also be "device.getAddress().isEqual(s.getMac())"
                            s.enable();
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                }
            }
        }
    };

    private View spinner;
    private HashMap<String, WifiDevice> wifiDevices;

    public static Fragment newInstance(int layout, User user, ArrayList<Sensor> sensors) {
        Fragment frag = new SensorsFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.ARG_LAYOUT, layout);
        args.putSerializable(Constants.ARG_USER, user);
        args.putParcelableArrayList(Constants.ARG_SENSORS, sensors);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // retrieve from bundle or savedInstanceState
        if (savedInstanceState == null) {
            Bundle args = getArguments();
            mLayoutId = args.getInt(Constants.ARG_LAYOUT);
            mUser = (User) args.getSerializable(Constants.ARG_USER);
            sensorsList = args.getParcelableArrayList(Constants.ARG_SENSORS);
        } else {
            mLayoutId = savedInstanceState.getInt(Constants.ARG_LAYOUT);
            mUser = (User) savedInstanceState.getSerializable(Constants.ARG_USER);
            sensorsList = savedInstanceState.getParcelableArrayList(Constants.ARG_SENSORS);
        }

        if(sensorsList == null) {
            sensorsList = new ArrayList<>();
        }

        btDevices = new HashMap<>();
        wifiDevices = new HashMap<>();

        // little hack to doesn't have options menu
        setHasOptionsMenu(true);

        // Listen for Bluetooth events
        ((MainActivity) getActivity()).registerBluetoothReceiver(mBluetoothReceiver);

        // Inflate the layout for this fragment
        return inflater.inflate(mLayoutId, container, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSwitch = (Switch) view.findViewById(R.id.mode_switch);

        listView = (ListView) view.findViewById(R.id.sensors_list);
        mAdapter = new SensorsAdapter(sensorsList, getContext());
        listView.setAdapter(mAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Sensor sensor = (Sensor) parent.getAdapter().getItem(position);
                if (sensor.isEnabled()) {
                    //TODO EDITER INFO
                }
            }
        });

        addBtn = (Button) view.findViewById(R.id.add_sensor_button);
        addBtn.setOnClickListener(new LinkANewSensor());

        refreshBtn = (Button) view.findViewById(R.id.refresh_button);
        refreshBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Start spinning the refresh btn
                refreshBtn.setVisibility(View.GONE);
                refreshBtnSpinner.setVisibility(View.VISIBLE);

                if (mSwitch.isChecked()) {
                    checkForWifi();
                    endOfBtnSpin();
                } else {
                    checkForBluetooth();
                }
            }
        });
        refreshBtnSpinner = (ProgressBar) view.findViewById(R.id.refresh_button_spinner);
        spinner = view.findViewById(R.id.sensor_spinner);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(Constants.ARG_LAYOUT, mLayoutId);
        outState.putSerializable(Constants.ARG_USER, mUser);
        outState.putParcelableArrayList(Constants.ARG_SENSORS, sensorsList);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void updateFragment(@Nullable User user, @Nullable ArrayList<Sensor> sensors, @Nullable ArrayList<Recording> recordings) {
        if(mUser != null)
            mUser = user;
        if(sensors != null) {
            sensorsList = sensors;
            mAdapter.notifyDataSetChanged();
        }
    }

    private void checkForWifi() {
        wifiDevices = refreshArp();
        if (wifiDevices.size() > 0) {
            Log.d(Constants.TAG, wifiDevices.get(wifiDevices.keySet().toArray()[0]).getIp() + " - " + wifiDevices.get(wifiDevices.keySet().toArray()[0]).getMac());
            String[] ip = wifiDevices.get(wifiDevices.keySet().toArray()[0]).getIp().split("\\.");

            int found = 0;
            for (Sensor s : sensorsList) {
                Log.d(Constants.TAG, s.getMac());
                if (wifiDevices.containsKey(s.getMac())) {
                    s.enable();
                    mAdapter.notifyDataSetChanged();
                    found++;
                }
            }
            if(found == 0) {
                Toast.makeText(getContext(), "No known sensor seems to be connected to your Wifi. Did you add it ?", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getContext(), "There aren't any device connected to your Wifi", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void endOfBtnSpin() {
        refreshBtn.setVisibility(View.VISIBLE);
        refreshBtnSpinner.setVisibility(View.GONE);
    }

    public void startSpinning() {
        spinner.setVisibility(View.VISIBLE);
    }

    public void endSpinning() {
        spinner.setVisibility(View.GONE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != REQUEST_ENABLE_BT)
            return;
        if (resultCode == RESULT_OK) {
            // Bluetooth has been enabled
            bluetoothDiscovery();
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.you_should_enable_bt), Toast.LENGTH_SHORT).show();
        }
    }

    private void saveDevice(String name, String room, SensorType type, Object device) {
        Sensor sensor;
        DatabaseReference sensorRef = FirebaseDatabase.getInstance().getReference("sensors").child(mUser.getHouse()).push();
        if(device instanceof BluetoothDevice) {
            sensor = new Sensor(sensorRef.getKey(), name, room, type, (BluetoothDevice) device, mUser.getUid());
        } else {
            sensor = new Sensor(sensorRef.getKey(), name, room, type, (WifiDevice) device, mUser.getUid());
        }

        sensorRef.setValue(sensor);

        sensorsList.add(sensor);
        mAdapter.notifyDataSetChanged();
    }

    private class LinkANewSensor implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            // wifi / bluetooth ?
            new AlertDialog.Builder(getContext())
                    .setTitle(getString(R.string.wifi_or_bluetooth_title))
                    .setMessage(getString(R.string.wifi_or_bluetooth_message))
                    .setPositiveButton(R.string.bluetooth, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            checkForBluetooth();
                            endOfDiscovery = false;
                            // We wait for the end of discovery
                            while(!endOfDiscovery); // TODO spinner

                            // display devices
                            displayDevices(btDevices);
                        }
                    })
                    .setNegativeButton(R.string.wifi, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            wifiDevices = refreshArp();

                            // display devices
                            displayDevices(wifiDevices);
                        }
                    })
                    .show();
        }

        private void displayDevices(final HashMap<String, ?> devices) {
            // setup the alert builder
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle("Choose the device :");

            // add a list
            final String[] devicesMac = devices.keySet().toArray(new String[devices.size()]);
            builder.setItems(devicesMac, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int index) {
                    setDeviceInfo(devices.get(devicesMac[index]));
                }
            });

            // create and show the alert dialog
            AlertDialog dialog = builder.create();
            dialog.show();
        }

        private void setDeviceInfo(final Object device) {
            LinearLayout layout = new LinearLayout(getContext());
            layout.setOrientation(LinearLayout.VERTICAL);
            layout.setPadding(50,0,50,0);

            final EditText nameBox = new EditText(getContext());
            nameBox.setHint("Sensor name");
            layout.addView(nameBox);

            final EditText roomBox = new EditText(getContext());
            roomBox.setHint("Room");
            layout.addView(roomBox);

            final Spinner typeBox = new Spinner(getContext());
            final ArrayAdapter adapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, SensorType.names());
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            typeBox.setAdapter(adapter);
            layout.addView(typeBox);

            AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
            dialog.setTitle("Sensor informations");
            dialog.setMessage("Set the sensor info (name, room and type) here :");
            dialog.setView(layout);
            dialog.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    saveDevice(nameBox.getText().toString(), roomBox.getText().toString(), SensorType.valueOf(typeBox.getSelectedItem().toString()), device);
                }
            });
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // do nothing
                }
            });
            dialog.show();
        }
    }
}
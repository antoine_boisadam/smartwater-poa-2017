package com.antoine.smartwater.activities;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.antoine.smartwater.R;
import com.antoine.smartwater.entities.Recording;
import com.antoine.smartwater.entities.Sensor;
import com.antoine.smartwater.entities.User;
import com.antoine.smartwater.utils.AuthListener;
import com.antoine.smartwater.utils.BluetoothService;
import com.antoine.smartwater.utils.Constants;
import com.antoine.smartwater.utils.MyFragmentInterface;
import com.antoine.smartwater.utils.BluetoothHandler;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String SELECTED_ITEM = "arg_selected_item";
    private static final int MY_PERMISSION_REQUEST_CONSTANT = 0;

    private BottomNavigationView mBottomNav;
    private int mSelectedItem;

    private User mUser;
    private ArrayList<Sensor> mSensors;
    private ArrayList<Recording> mRecordings;

    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseAuth.AuthStateListener mAuthListener;

    private DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

    // Shared variables
    private BluetoothService mBluetoothService = null;
    private BroadcastReceiver currentReceiver = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Auth
        mAuthListener = new AuthListener();

        FirebaseUser user = mAuth.getCurrentUser();
        if(user == null) {
            // Should never happen
            Intent intent = new Intent(MainActivity.this, RouterActivity.class);
            startActivity(intent);
        }

        Intent intent = getIntent();
        mUser = (User) intent.getSerializableExtra(Constants.INTENT_USER);
        if(mUser == null) {
            mUser = new User(user);
        }
        mSensors = new ArrayList<>();
        mRecordings = new ArrayList<>();

        setContentView(R.layout.activity_main);

        mBottomNav = (BottomNavigationView) findViewById(R.id.navigation);
        if(mUser.isAdmin()) {
            mBottomNav.getMenu().clear();
            mBottomNav.inflateMenu(R.menu.bottom_nav_items_admin);
        }
        mBottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                selectFragment(item);
                return true;
            }
        });

        MenuItem selectedItem;
        if (savedInstanceState != null) {
            mSelectedItem = savedInstanceState.getInt(SELECTED_ITEM, 0);
            selectedItem = mBottomNav.getMenu().findItem(mSelectedItem);
        } else {
            selectedItem = mBottomNav.getMenu().getItem(0);
        }
        selectFragment(selectedItem);

        // Listen for db update
        mDatabase.child("users").child(user.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mUser = dataSnapshot.getValue(User.class);

                MyFragmentInterface frag = (MyFragmentInterface) getSupportFragmentManager().findFragmentById(R.id.container);
                if(frag != null) {
                    frag.updateFragment(mUser, null, null);
                    Log.i(Constants.TAG, "User updated");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(Constants.TAG, "onCancelled: error while fetching user");
            }
        });
        mDatabase.child("users").child(user.getUid()).keepSynced(true);

        mDatabase.child("sensors").child(mUser.getHouse()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mSensors.clear();
                for (DataSnapshot sensorSnapshot: dataSnapshot.getChildren()) {
                    mSensors.add(sensorSnapshot.getValue(Sensor.class));
                }

                MyFragmentInterface frag = (MyFragmentInterface) getSupportFragmentManager().findFragmentById(R.id.container);
                if(frag != null) {
                    frag.updateFragment(null, mSensors, null);
                    Log.i(Constants.TAG, "Sensor list updated");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(Constants.TAG, "onCancelled: error while fetching sensor list");
            }
        });
        mDatabase.child("sensors").child(mUser.getHouse()).keepSynced(true);

        mDatabase.child("recordings").child(mUser.getHouse()).child(mUser.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mRecordings.clear();
                for (DataSnapshot recordingsSnapshot: dataSnapshot.getChildren()) {
                    for(DataSnapshot recordingSnapshot : recordingsSnapshot.getChildren()) {
                        mRecordings.add(recordingSnapshot.getValue(Recording.class));
                    }
                }

                MyFragmentInterface frag = (MyFragmentInterface) getSupportFragmentManager().findFragmentById(R.id.container);
                if(frag != null) {
                    frag.updateFragment(null, null, mRecordings);
                    Log.i(Constants.TAG, "Recording list updated");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(Constants.TAG, "onCancelled: error while fetching recording list");
            }
        });
        mDatabase.child("recordings").child(mUser.getHouse()).child(mUser.getUid()).keepSynced(true);



        // We need to use this to use bluetooth on Marshmallow devices
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},MY_PERMISSION_REQUEST_CONSTANT);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(SELECTED_ITEM, mSelectedItem);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        MenuItem homeItem = mBottomNav.getMenu().getItem(0);
        if (mSelectedItem != homeItem.getItemId()) {
            // select home item
            selectFragment(homeItem);
        } else {
            super.onBackPressed();
        }
    }

    private void selectFragment(MenuItem item) {
        Fragment frag = null;
        // init corresponding fragment
        switch (item.getItemId()) {
            case R.id.menu_home:
                frag = UsageFragment.newInstance(R.layout.fragment_home, mUser, mRecordings);
                break;
            case R.id.menu_history:
                frag = HistoryFragment.newInstance(R.layout.fragment_history, mUser, mRecordings);
                break;
            case R.id.menu_add_record:
                frag = AddRecordFragment.newInstance(R.layout.fragment_add_record, mUser, mSensors);
                break;
            case R.id.menu_sensors:
                frag = SensorsFragment.newInstance(R.layout.fragment_sensors, mUser, mSensors);
                break;
        }

        // update selected item
        mSelectedItem = item.getItemId();

        // uncheck the other items.
        for (int i = 0; i< mBottomNav.getMenu().size(); i++) {
            MenuItem menuItem = mBottomNav.getMenu().getItem(i);
            menuItem.setChecked(menuItem.getItemId() == item.getItemId());
        }

        updateToolbarText(item.getTitle());

        if (frag != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(R.id.container, frag, frag.getTag());
            ft.commit();
        }
    }

    private void updateToolbarText(CharSequence text) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(text);
        }
    }

    public BluetoothService connectToBluetooth(BluetoothDevice device, Sensor sensor) {
        return connectToBluetooth(new BluetoothHandler(this, sensor), device);
    }

    public BluetoothService connectToBluetooth(Handler handler, BluetoothDevice device) {

        if(mBluetoothService == null) {
            mBluetoothService = new BluetoothService(handler, device);
            mBluetoothService.connect();
        } else {
            Log.d(Constants.TAG, "Already connected to " + mBluetoothService.getDevice().getAddress());
            if(mBluetoothService.getDevice().getAddress() != device.getAddress()) {
                //We disconnect the other device
                mBluetoothService.stop();

                // And connect the one gave in params
                mBluetoothService = new BluetoothService(handler, device);
                mBluetoothService.connect();
            }
        }

        return mBluetoothService;
    }

    public void addRecord(final float value, final Sensor sensor) {
        // Stop spinning
        //((AddRecordFragment) (getSupportFragmentManager().findFragmentById(R.id.container))).endSpinning();//TODO

        if(value == 0) {
            Toast.makeText(getApplicationContext(), R.string.invalid_value, Toast.LENGTH_SHORT).show();
            return;
        }
        new AlertDialog.Builder(MainActivity.this)
                .setTitle(getString(R.string.new_recording))
                .setMessage(getString(R.string.ask_before_store) + "\n" + value + " " + getString(R.string.liters))
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // We create a new record
                        Recording r = new Recording(value, sensor, System.currentTimeMillis(), mUser.getUid());
                        // And add it to the list
                        mRecordings.add(r);

                        try {
                            // Then, we reset the liter counter
                            ((AddRecordFragment) (getSupportFragmentManager().findFragmentById(R.id.container))).resetSensor(sensor);

                            // And add the new recording in DB
                            mDatabase.child("recordings").child(mUser.getHouse()).child(mUser.getUid()).child(sensor.getId()).push().setValue(r);
                        } catch(Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), R.string.not_saved, Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton(R.string.not_now, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .show();
    }

    // Needed to use bluetooth on Marshmallow devices
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_REQUEST_CONSTANT: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //permission granted!
                }
                return;
            }
        }
    }

    public BluetoothService getBluetoothService() {
        return mBluetoothService;
    }

    public void registerBluetoothReceiver(BroadcastReceiver receiver) {
        // Unregister last receiver if exist
        if(currentReceiver != null) {
            unregisterReceiver(currentReceiver);
        }

        // Bluetooth receiver
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);

        // Register the new receiver
        currentReceiver = receiver;
        registerReceiver(currentReceiver, filter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Auth State Listener
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAuth.removeAuthStateListener(mAuthListener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mBluetoothService != null) {
            mBluetoothService.stop();
        }
        if(currentReceiver != null) {
            unregisterReceiver(currentReceiver);
        }
    }
}
package com.antoine.smartwater.activities;

/**
 * Created by antoine on 16/05/17.
 */


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.antoine.smartwater.R;
import com.antoine.smartwater.entities.Recording;
import com.antoine.smartwater.entities.Sensor;
import com.antoine.smartwater.entities.User;
import com.antoine.smartwater.utils.Constants;
import com.antoine.smartwater.utils.MyFragmentInterface;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.animation.PieChartRotationAnimatorV14;
import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.view.PieChartView;


public class UsageFragment extends Fragment implements MyFragmentInterface {

    private int mLayoutId;

    private TextView mTotalTextView;
    private PieChartView mPieChart;

    private User mUser;
    private ArrayList<Recording> recordingList;

    private View usage_layout, no_records_layout;

    public static Fragment newInstance(int layout, User user, ArrayList<Recording> recordings) {
        Fragment frag = new UsageFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.ARG_LAYOUT, layout);
        args.putSerializable(Constants.ARG_USER, user);
        args.putParcelableArrayList(Constants.ARG_RECORDINGS, recordings);
        frag.setArguments(args);
        return frag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // retrieve text and color from bundle or savedInstanceState
        if (savedInstanceState == null) {
            Bundle args = getArguments();
            mLayoutId = args.getInt(Constants.ARG_LAYOUT);
            mUser = (User) args.getSerializable(Constants.ARG_USER);
            recordingList = args.getParcelableArrayList(Constants.ARG_RECORDINGS);
        } else {
            mLayoutId = savedInstanceState.getInt(Constants.ARG_LAYOUT);
            mUser = (User) savedInstanceState.getSerializable(Constants.ARG_USER);
            recordingList = savedInstanceState.getParcelableArrayList(Constants.ARG_RECORDINGS);
        }
        setHasOptionsMenu(true);

        // Inflate the layout for this fragment
        return inflater.inflate(mLayoutId, container, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.logout_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.option_logout:
                FirebaseAuth.getInstance().signOut();
                Toast.makeText(getContext(), R.string.logout_successful, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        usage_layout = view.findViewById(R.id.usage_layout);
        no_records_layout = view.findViewById(R.id.no_records);
        if(recordingList == null || recordingList.size() == 0) {
            usage_layout.setVisibility(View.GONE);
            no_records_layout.setVisibility(View.VISIBLE);
        }

        // Working with chart
        mPieChart = (PieChartView) view.findViewById(R.id.pie_chart);
        mPieChart.setChartRotationEnabled(false);

        // Working with comsuption value
        mTotalTextView = (TextView) view.findViewById(R.id.consumption_value);

        // And init them
        initView(mPieChart, mTotalTextView);
    }

    private void initView(PieChartView chart, TextView textView) {
        if(recordingList == null) return;

        // We calculate quantity of water used for each type of sensors (TAP, SHOWER and UNKNOWN)
        float tap = 0, shower = 0, unknown = 0;
        ArrayList<SliceValue> sliceValues = new ArrayList<>();
        for (Recording r : recordingList) {
            switch (r.getSensorType()) {
                case TAP:
                    tap += r.getConsumption();
                    break;
                case SHOWER:
                    shower += r.getConsumption();
                    break;
                case UNKNOWN:
                    unknown += r.getConsumption();
                    break;
                default:
                    unknown += r.getConsumption();
            }
        }

        // We create three SliceValue (one per type)
        if(tap > 0) {
            SliceValue tap_value = new SliceValue(tap, getResources().getColor(R.color.color_chart_tap));
            tap_value.setLabel(getResources().getString(R.string.tap_legend));
            sliceValues.add(tap_value);
        }

        if(shower > 0) {
            SliceValue shower_value = new SliceValue(shower, getResources().getColor(R.color.color_chart_shower));
            shower_value.setLabel(getResources().getString(R.string.shower_legend));
            sliceValues.add(shower_value);
        }

        if(unknown > 0) {
            SliceValue unknown_value = new SliceValue(unknown, getResources().getColor(R.color.color_chart_unknown));
            unknown_value.setLabel(getResources().getString(R.string.unknow_legend));
            sliceValues.add(unknown_value);
        }

        // Create a PieChartData Object with the list previously filled
        PieChartData data = new PieChartData(sliceValues);
        data.setHasLabels(true);

        // And finally set it to the chart
        chart.setPieChartData(data);

        // We also set the text value :
        textView.setText((tap + shower + unknown) + " " + getResources().getString(R.string.liters));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(Constants.ARG_LAYOUT, mLayoutId);
        outState.putSerializable(Constants.ARG_USER, mUser);
        outState.putParcelableArrayList(Constants.ARG_RECORDINGS, recordingList);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void updateFragment(@Nullable User user, @Nullable ArrayList<Sensor> sensors, @Nullable ArrayList<Recording> recordings) {
        if(user != null) {
            mUser = user;
        }

        if(recordings != null) {
            if (recordings.size() == 0) {
                usage_layout.setVisibility(View.GONE);
                no_records_layout.setVisibility(View.VISIBLE);
            } else {
                usage_layout.setVisibility(View.VISIBLE);
                no_records_layout.setVisibility(View.GONE);
            }
            recordingList = recordings;

            // Reinit view
            initView(mPieChart, mTotalTextView);
        }
    }
}
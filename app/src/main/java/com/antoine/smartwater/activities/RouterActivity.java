package com.antoine.smartwater.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.antoine.smartwater.R;
import com.antoine.smartwater.entities.User;
import com.antoine.smartwater.utils.AuthListener;
import com.antoine.smartwater.utils.Constants;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * Created by antoine on 22/05/17.
 */

public class RouterActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading);

        // Offline persistence
        try {
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        } catch(RuntimeException e) {
            Log.e(Constants.TAG, "Persistence is already enabled");
        }

        // Firebase
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new AuthListener();
        FirebaseUser fbUser = mAuth.getCurrentUser();

        if (fbUser != null) {
            // Init db
            mDatabase = FirebaseDatabase.getInstance().getReference("users").child(fbUser.getUid());
            mDatabase.keepSynced(true);

            // Retrieving data
            ValueEventListener userListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    User u = dataSnapshot.getValue(User.class);

                    Intent intent = new Intent(RouterActivity.this, MainActivity.class);
                    intent.putExtra(Constants.INTENT_USER, u);
                    startActivity(intent);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    // Getting User failed, log a message
                    Log.w(Constants.TAG, "loadUser:onCancelled", databaseError.toException());
                }
            };
            mDatabase.addListenerForSingleValueEvent(userListener);
        } else {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAuth.removeAuthStateListener(mAuthListener);
    }
}

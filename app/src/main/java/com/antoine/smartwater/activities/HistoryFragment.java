package com.antoine.smartwater.activities;

/**
 * Created by antoine on 16/05/17.
 */


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.antoine.smartwater.R;
import com.antoine.smartwater.adapters.RecordingAdapter;
import com.antoine.smartwater.entities.Recording;
import com.antoine.smartwater.entities.Sensor;
import com.antoine.smartwater.entities.User;
import com.antoine.smartwater.utils.Constants;
import com.antoine.smartwater.utils.MyFragmentInterface;
import com.antoine.smartwater.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.gesture.ZoomType;
import lecho.lib.hellocharts.listener.ColumnChartOnValueSelectListener;
import lecho.lib.hellocharts.listener.ViewportChangeListener;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.SubcolumnValue;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.ColumnChartView;
import lecho.lib.hellocharts.view.PreviewColumnChartView;

public class HistoryFragment extends Fragment implements MyFragmentInterface {

    private int mLayoutId;

    private ListView listView;

    private PreviewColumnChartView previewChartView;
    private ColumnChartView chartView;

    private ArrayList<Recording> recordingList;
    private RecordingAdapter mAdapter;

    private User mUser;

    private View history_layout, no_records_layout;

    public static Fragment newInstance(int layout, User user, ArrayList<Recording> recordings) {
        Fragment frag = new HistoryFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.ARG_LAYOUT, layout);
        args.putSerializable(Constants.ARG_USER, user);
        args.putParcelableArrayList(Constants.ARG_RECORDINGS, recordings);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // retrieve text and color from bundle or savedInstanceState
        if (savedInstanceState == null) {
            Bundle args = getArguments();
            mLayoutId = args.getInt(Constants.ARG_LAYOUT);
            mUser = (User) args.getSerializable(Constants.ARG_USER);
            recordingList = getAllRecordingsByDay(args.<Recording>getParcelableArrayList(Constants.ARG_RECORDINGS));
        } else {
            mLayoutId = savedInstanceState.getInt(Constants.ARG_LAYOUT);
            mUser = (User) savedInstanceState.getSerializable(Constants.ARG_USER);
            recordingList = getAllRecordingsByDay(savedInstanceState.<Recording>getParcelableArrayList(Constants.ARG_RECORDINGS));
        }

        setHasOptionsMenu(true);

        // Inflate the layout for this fragment
        return inflater.inflate(mLayoutId, container, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.history_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int days;
        switch (item.getItemId()) {
            case R.id.option_last_week:
                days = 7;
                break;
            case R.id.option_last_month:
                days = 30;
                break;
            case R.id.option_menu_all_time:
                days = -1;
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        previewChartView.setCurrentViewport(getViewport(days, recordingList.size(), chartView.getMaximumViewport()));
        return true;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        history_layout = view.findViewById(R.id.history_layout);
        no_records_layout = view.findViewById(R.id.no_records);
        if(recordingList.size() == 0) {
            history_layout.setVisibility(View.GONE);
            no_records_layout.setVisibility(View.VISIBLE);
        }

        // List view (old design)
        listView = (ListView) view.findViewById(R.id.history_list);
        mAdapter = new RecordingAdapter(recordingList, getContext());
        listView.setAdapter(mAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Recording item = (Recording) parent.getAdapter().getItem(position);
                Toast.makeText(getContext(), item.getConsumption() + " " + getString(R.string.liters), Toast.LENGTH_LONG).show();
            }
        });

        // Chart view
        chartView = (ColumnChartView) view.findViewById(R.id.column_chart);
        previewChartView = (PreviewColumnChartView) view.findViewById(R.id.chart_preview);

        initChart(chartView, previewChartView);
    }

    private void initChart(ColumnChartView chart, PreviewColumnChartView previewChart) {
        List<Column> dataList = new ArrayList<>();
        List<AxisValue> axisValues = new ArrayList<>();
        List<SubcolumnValue> values;
        Recording r;

        for (int i = 0; i < recordingList.size(); i++) {
            r = recordingList.get(i);

            values = new ArrayList<>();
            values.add(new SubcolumnValue(r.getConsumption(), ChartUtils.pickColor()));
            dataList.add(new Column(values));

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM");
            String formattedDate = dateFormat.format(r.getDate());

            AxisValue axisValue = new AxisValue(i);
            axisValue.setLabel(formattedDate);
            axisValues.add(axisValue);
        }

        ColumnChartData chartData = new ColumnChartData(dataList);
        chartData.setAxisXBottom(new Axis(axisValues));
        chartData.setAxisYLeft(new Axis().setHasLines(true));


        chart.setColumnChartData(chartData);
        chart.setZoomEnabled(false);
        chart.setScrollEnabled(false);
        chart.setOnValueTouchListener(new ColumnChartOnValueSelectListener() {
            @Override
            public void onValueDeselected() {

            }

            @Override
            public void onValueSelected(int columnIndex, int subcolumnIndex, SubcolumnValue value) {
                Recording r = recordingList.get(columnIndex);
                Toast.makeText(getContext(), getString(R.string.consumption_on_day, r.getConsumption(), r.getDate()), Toast.LENGTH_SHORT).show();
            }
        });


        ColumnChartData previewData = new ColumnChartData(chartData);
        for (Column column : previewData.getColumns()) {
            for (SubcolumnValue value : column.getValues()) {
                value.setColor(ChartUtils.DEFAULT_DARKEN_COLOR);
            }
        }
        previewChart.setColumnChartData(previewData);
        previewChart.setViewportChangeListener(new ViewportListener());
        previewChart.setCurrentViewport(getViewport(15, recordingList.size(), chart.getMaximumViewport()));
        previewChart.setZoomType(ZoomType.HORIZONTAL);
    }

    private Viewport getViewport(int numberOfDays, int totalDays, Viewport maximumViewport) {
        Viewport tempViewport = new Viewport(maximumViewport);
        if(numberOfDays > 0) {
            float dx = numberOfDays * (tempViewport.width() / totalDays);
            if(dx < tempViewport.width())
                tempViewport.offsetTo(tempViewport.width() - dx, tempViewport.height());
        }

        return tempViewport;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(Constants.ARG_LAYOUT, mLayoutId);
        outState.putSerializable(Constants.ARG_USER, mUser);
        outState.putParcelableArrayList(Constants.ARG_RECORDINGS, recordingList);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void updateFragment(@Nullable User user, @Nullable ArrayList<Sensor> sensors, @Nullable ArrayList<Recording> recordings) {
        if(user != null) {
            mUser = user;
        }
        if(recordings != null) {
            recordingList = recordings;

            if (recordingList.size() == 0) {
                history_layout.setVisibility(View.GONE);
                no_records_layout.setVisibility(View.VISIBLE);
            } else {
                history_layout.setVisibility(View.VISIBLE);
                no_records_layout.setVisibility(View.GONE);
            }

            initChart(chartView, previewChartView);
            mAdapter.notifyDataSetChanged();
        }
    }

    private class ViewportListener implements ViewportChangeListener {

        @Override
        public void onViewportChanged(Viewport newViewport) {
            chartView.setCurrentViewport(newViewport);
        }
    }

    private ArrayList<Recording> getAllRecordingsByDay(ArrayList<Recording> recordings) {
        ArrayList<Recording> daily = new ArrayList<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyy");

        if(recordings == null || recordings.size() == 0) {
            return daily;
        }

        int i = 0; // current formatted index (equivalent to formatted.size()-1)
        daily.add(recordings.get(0));
        // Concat all same days records + append missing days between 2 records
        for(int j = 1; j < recordings.size(); j++) {
            if(dateFormat.format(daily.get(i).getDate()).equals(dateFormat.format(recordings.get(j).getDate()))) {
                // Same day
                Recording r = daily.get(i);
                r.setConsumption(recordings.get(j).getConsumption()+r.getConsumption());
                daily.set(i, r);
            } else {
                // Other day

                // Try to append some day between
                for(long k = (daily.get(i).getDate()/Constants.ONE_DAY)+1; k < recordings.get(j).getDate()/Constants.ONE_DAY; k++) {
                    daily.add(new Recording(0, new Sensor(), k*Constants.ONE_DAY + Constants.ONE_DAY/2, null)); // We add half a day
                    i++;
                }

                // And we add our recording
                daily.add(recordings.get(j));
                i++;
            }
        }

        // Append empty recording from last one to today
        long now = System.currentTimeMillis();
        for(long k = (daily.get(i).getDate()/Constants.ONE_DAY)+1; k < now/Constants.ONE_DAY; k++) {
            daily.add(new Recording(0, new Sensor(), k*Constants.ONE_DAY + Constants.ONE_DAY/2, null)); // We add half a day
        }

        // If there are less than 14 days, we append empty ones before (design question)
        for(int l = daily.size(); l < 14; l++) {
            daily.add(0, new Recording(0, new Sensor(), System.currentTimeMillis() - l*Constants.ONE_DAY, null));
        }

        return daily;
    }

    // Return only days with recordings
    private ArrayList<Recording> getRecordingsByDay(ArrayList<Recording> recordings) {
        ArrayList<Recording> daily = new ArrayList<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyy");

        if(recordings.size() == 0) {
            return daily;
        }

        int i = 0; // current formatted index (equivalent to formatted.size()-1)
        daily.add(recordings.get(0));
        // Concat all same days records + append missing days between 2 records
        for(int j = 1; j < recordings.size(); j++) {
            if(dateFormat.format(daily.get(i).getDate()).equals(dateFormat.format(recordings.get(j).getDate()))) {
                // Same day
                Recording r = daily.get(i);
                r.setConsumption(recordings.get(j).getConsumption()+r.getConsumption());
                daily.set(i, r);
            } else {
                // And we add our recording
                daily.add(recordings.get(j));
                i++;
            }
        }

        return daily;
    }
}
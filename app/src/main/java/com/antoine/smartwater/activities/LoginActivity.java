package com.antoine.smartwater.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.antoine.smartwater.R;
import com.antoine.smartwater.entities.House;
import com.antoine.smartwater.entities.User;
import com.antoine.smartwater.utils.AuthListener;
import com.antoine.smartwater.utils.Constants;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by antoine on 22/05/17.
 */

public class LoginActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new AuthListener();
    }

    public void createAccount(View view) {
        EditText emailEditText = (EditText) findViewById(R.id.field_email);
        final String email = emailEditText.getText().toString();

        EditText passwordEditText = (EditText) findViewById(R.id.field_password);
        String password = passwordEditText.getText().toString();

        if(email.isEmpty() || password.isEmpty()) {
            Toast.makeText(getApplicationContext(), R.string.email_password_empty, Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = new Intent(LoginActivity.this, NewAccountActivity.class);
        intent.putExtra(Constants.INTENT_EMAIL, email);
        intent.putExtra(Constants.INTENT_PASSWORD, password);
        startActivity(intent);
    }

    public void signIn(View view) {
        EditText emailEditText = (EditText) findViewById(R.id.field_email);
        final String email = emailEditText.getText().toString();

        EditText passwordEditText = (EditText) findViewById(R.id.field_password);
        String password = passwordEditText.getText().toString();

        if(email.isEmpty() || password.isEmpty()) {
            Toast.makeText(getApplicationContext(), R.string.email_password_empty, Toast.LENGTH_SHORT).show();
            return;
        }

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(Constants.TAG, "signInWithEmail:onComplete:" + task.isSuccessful());

                        if (!task.isSuccessful()) {
                            Log.w(Constants.TAG, "signInWithEmail:failed", task.getException());
                            Toast.makeText(LoginActivity.this, R.string.auth_failed, Toast.LENGTH_SHORT).show();
                            return;
                        }

                        // Go to RouterActivity (That will redirect to MainActivity)
                        Intent intent = new Intent(LoginActivity.this, RouterActivity.class);
                        startActivity(intent);
                    }
                });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAuth.removeAuthStateListener(mAuthListener);
    }
}

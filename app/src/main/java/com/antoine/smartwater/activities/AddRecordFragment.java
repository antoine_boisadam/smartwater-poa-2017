package com.antoine.smartwater.activities;

/**
 * Created by antoine on 16/05/17.
 */


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.antoine.smartwater.R;
import com.antoine.smartwater.adapters.SensorsAdapter;
import com.antoine.smartwater.entities.Recording;
import com.antoine.smartwater.entities.Sensor;
import com.antoine.smartwater.entities.User;
import com.antoine.smartwater.utils.BluetoothService;
import com.antoine.smartwater.utils.Constants;
import com.antoine.smartwater.utils.DeviceManagementFragment;
import com.antoine.smartwater.utils.MyFragmentInterface;
import com.antoine.smartwater.utils.WifiDevice;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;

import lecho.lib.hellocharts.model.Line;

import static com.antoine.smartwater.utils.Constants.STATE_CONNECTED;
import static com.antoine.smartwater.utils.Constants.STATE_ERROR;

public class AddRecordFragment extends DeviceManagementFragment implements MyFragmentInterface, View.OnClickListener, AdapterView.OnItemClickListener {

    private int mLayoutId;
    private ArrayList<Sensor> sensorList;
    private User mUser;

    private HashMap<String, BluetoothDevice> btDevices;
    private HashMap<String, WifiDevice> wifiDevices;

    private ArrayList<BaseAdapter> mAdaptaters;

    private FloatingActionButton refreshBtn;
    private ViewGroup add_record_layout, no_records_layout, sensor_list_layout;

    private Sensor clickedSensor;

    private final BroadcastReceiver mBluetoothReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                Log.d(Constants.TAG, "AddRecordFragment.onReceive: " + "Discovery started");
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                //discovery finishes
                Log.d(Constants.TAG, "AddRecordFragment.onReceive: " + "Discovery finishes");
                endOfBtnSpin();
            } else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                //bluetooth device found
                Log.d(Constants.TAG, "AddRecordFragment.onReceive: " + "Device found");
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (!btDevices.containsKey(device.getAddress())) {
                    btDevices.put(device.getAddress(), device);
                    for (Sensor s : sensorList) {
                        Log.d(Constants.TAG, s.getMac());
                        if (btDevices.containsKey(s.getMac())) { // Can also be "device.getAddress().isEqual(s.getMac())"
                            Log.d(Constants.TAG, "AddRecordFragment.onReceive: " + "Device exists and is now enabled");
                            s.enable();
                            // Notify each sensor adapter
                            for(BaseAdapter adapter: mAdaptaters) {
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                }
            }
        }
    };

    public static Fragment newInstance(int layout, User user, ArrayList<Sensor> sensors) {
        Fragment frag = new AddRecordFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.ARG_LAYOUT, layout);
        args.putSerializable(Constants.ARG_USER, user);
        args.putParcelableArrayList(Constants.ARG_SENSORS, sensors);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // retrieve text and color from bundle or savedInstanceState
        if (savedInstanceState == null) {
            Bundle args = getArguments();
            mLayoutId = args.getInt(Constants.ARG_LAYOUT);
            mUser = (User) args.getSerializable(Constants.ARG_USER);
            sensorList = args.getParcelableArrayList(Constants.ARG_SENSORS);
        } else {
            mLayoutId = savedInstanceState.getInt(Constants.ARG_LAYOUT);
            mUser = (User) savedInstanceState.getSerializable(Constants.ARG_USER);
            sensorList = savedInstanceState.getParcelableArrayList(Constants.ARG_SENSORS);
        }

        if(sensorList == null) {
            sensorList = new ArrayList<>();
        }

        btDevices = new HashMap<>();
        wifiDevices = new HashMap<>();
        mAdaptaters = new ArrayList<>();

        // little hack to doesn't have options menu
        setHasOptionsMenu(true);

        // Listen for Bluetooth events
        ((MainActivity) getActivity()).registerBluetoothReceiver(mBluetoothReceiver);

        // Inflate the layout for this fragment
        return inflater.inflate(mLayoutId, container, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        add_record_layout = (CoordinatorLayout) view.findViewById(R.id.add_record_layout);
        no_records_layout = (RelativeLayout) view.findViewById(R.id.no_records);
        sensor_list_layout = (LinearLayout) view.findViewById(R.id.sensor_list_layout);

        if(sensorList.size() == 0) {
            add_record_layout.setVisibility(View.GONE);
            no_records_layout.setVisibility(View.VISIBLE);
        }

        refreshBtn = (FloatingActionButton) view.findViewById(R.id.fab_refresh_button);
        refreshBtn.setOnClickListener(this);

        initView();
    }

    private void initView() {
        sensor_list_layout.removeAllViews();
        ArrayList<String> rooms = new ArrayList<>();
        for(Sensor s : sensorList) {
            if(!rooms.contains(s.getRoom().toLowerCase())) {
                rooms.add(s.getRoom().toLowerCase());
            }
        }

        for(String room : rooms) {
            LinearLayout layout = new LinearLayout(getContext());
            layout.setOrientation(LinearLayout.VERTICAL);

            // Room title
            TextView roomTitle = new TextView(getContext());
            roomTitle.setText(capitalize(room));
            roomTitle.setTextSize(18);
            roomTitle.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));

            // List View
            ListView listView = new ListView(getContext());
            ArrayList<Sensor> roomSensorList = new ArrayList<>();
            for(Sensor s : sensorList) {
                if(s.getRoom().toLowerCase().equals(room)) {
                    roomSensorList.add(s);
                }
            }
            SensorsAdapter adapter = new SensorsAdapter(roomSensorList, getContext());
            mAdaptaters.add(adapter);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(this);

            layout.addView(roomTitle);
            layout.addView(listView);

            sensor_list_layout.addView(layout);
        }
    }

    private String capitalize(String str) {
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    private void checkForWifi() {
        wifiDevices = refreshArp();
        if (wifiDevices.size() > 0) {
            int found = 0;
            for (Sensor s : sensorList) {
                Log.d(Constants.TAG, s.getMac());
                if (wifiDevices.containsKey(s.getMac())) {
                    s.enable();
                    for(BaseAdapter adapter: mAdaptaters)
                        adapter.notifyDataSetChanged();
                    found++;
                }
            }
            if(found == 0) {
                Toast.makeText(getContext(), "No known sensor seems to be connected to your Wifi. Ask your house admin for more information.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getContext(), "There aren't any device connected to your Wifi", Toast.LENGTH_SHORT).show();
        }
    }

    private String sendRequest(String ipAddress, String route) {
        String serverResponse = "ERROR";

        try {

            HttpClient httpclient = new DefaultHttpClient(); // create an HTTP client
            URI website = new URI("http://" + ipAddress + route);
            HttpGet getRequest = new HttpGet(); // create an HTTP GET object
            getRequest.setURI(website); // set the URL of the GET request
            HttpResponse response = httpclient.execute(getRequest); // execute the request
            // get the ip address server's reply
            InputStream content = null;
            content = response.getEntity().getContent();
            BufferedReader in = new BufferedReader(new InputStreamReader(content));
            serverResponse = in.readLine();
            // Close the connection
            content.close();
        } catch (ClientProtocolException e) {
            // HTTP error
            serverResponse = e.getMessage();
            e.printStackTrace();
        } catch (IOException e) {
            // IO error
            serverResponse = e.getMessage();
            e.printStackTrace();
        } catch (URISyntaxException e) {
            // URL syntax error
            serverResponse = e.getMessage();
            e.printStackTrace();
        }
        // return the server's reply/response text
        return serverResponse;
    }

    public void resetSensor(Sensor sensor) {
        if (sensor.getMode() == Sensor.Mode.WIFI) {
            String ipAddress = wifiDevices.get(sensor.getMac()).getIp();
            String route = "";
            // We request the server
            new HttpRequestAsyncTask(getContext(), ipAddress, route).execute();
        } else {
            MainActivity ma = (MainActivity) getActivity();
            BluetoothService bluetoothConnection = ma.getBluetoothService();
            bluetoothConnection.write("1".getBytes());
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(Constants.ARG_LAYOUT, mLayoutId);
        outState.putSerializable(Constants.ARG_USER, mUser);
        outState.putParcelableArrayList(Constants.ARG_SENSORS, sensorList);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void updateFragment(@Nullable User user, @Nullable ArrayList<Sensor> sensors, @Nullable ArrayList<Recording> recordings) {
        if(user != null) {
            mUser = user;
        }
        if(sensors != null) {
            sensorList = sensors;

            if (sensorList.size() == 0) {
                add_record_layout.setVisibility(View.GONE);
                no_records_layout.setVisibility(View.VISIBLE);
            } else {
                add_record_layout.setVisibility(View.VISIBLE);
                no_records_layout.setVisibility(View.GONE);
                initView();
            }
        }
    }

    @Override
    protected void endOfBtnSpin() {
        ((AnimatedVectorDrawable) refreshBtn.getDrawable()).stop();
    }

    @Override
    public void onClick(View v) {
        // ON CLICK FAB BTN
        ((AnimatedVectorDrawable) refreshBtn.getDrawable()).start();
        checkForWifi(); // TODO
        checkForBluetooth();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // ON CLICK LIST ITEN
        clickedSensor = (Sensor) parent.getAdapter().getItem(position);
        if (clickedSensor.isEnabled()) {
            // Starting spinning
            //startSpinning(); // TODO

            if (clickedSensor.getMode() == Sensor.Mode.WIFI) {
                String ipAddress = wifiDevices.get(clickedSensor.getMac()).getIp();
                String route = "/CONSULTATION";
                // We request the server
                new HttpRequestAsyncTask(getContext(), ipAddress, route).execute();
            } else {
                BluetoothDevice device = btDevices.get(clickedSensor.getMac());
                // On se connecte
                MainActivity ma = (MainActivity) getActivity();
                BluetoothService bluetoothConnection = ma.connectToBluetooth(device, clickedSensor);

                // Consulter et sauvegarder
                while (bluetoothConnection.getState() != STATE_CONNECTED && bluetoothConnection.getState() != STATE_ERROR) {
                }
                bluetoothConnection.write("2".getBytes());
            }
        }
    }

    /**
     * An AsyncTask is needed to execute HTTP requests in the background so that they do not
     * block the user interface.
     */
    private class HttpRequestAsyncTask extends AsyncTask<Void, Void, Void> {

        // declare variables needed
        private String requestReply, ipAddress, route;
        private Context context;

        /**
         * Description: The asyncTask class constructor. Assigns the values used in its other methods.
         *
         * @param ipAddress the ip address to send the request to
         * @param route     the route accessed
         */
        public HttpRequestAsyncTask(Context context, String ipAddress, String route) {
            this.context = context;

            this.ipAddress = ipAddress;
            this.route = route;
        }

        /**
         * Name: doInBackground
         * Description: Sends the request to the ip address
         *
         * @param voids
         * @return
         */
        @Override
        protected Void doInBackground(Void... voids) {
            // Data sent, waiting for reply from server...
            requestReply = sendRequest(ipAddress, route);
            return null;
        }

        /**
         * Name: onPostExecute
         * Description: This function is executed after the HTTP request returns from the ip address.
         *
         * @param aVoid void parameter
         */
        @Override
        protected void onPostExecute(Void aVoid) {
            // Response
            Log.d(Constants.TAG, requestReply);
            float val;
            try {
                val = Float.valueOf(requestReply);
            } catch (Exception e) {
                val = 0;
            }
            if (!route.contains("CONSULTATION")) {
                // We save it
                try {
                    ((MainActivity) getActivity()).addRecord(val, clickedSensor);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                // We just print it
                Toast.makeText(context, String.valueOf(val), Toast.LENGTH_SHORT).show();
            }
        }

        /**
         * Name: onPreExecute
         * Description: This function is executed before the HTTP request is sent to ip address.
         * The function will set the dialog's message and display the dialog.
         */
        @Override
        protected void onPreExecute() {
            // Sending data to server, please wait...
        }

    }
}
package com.antoine.smartwater.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.antoine.smartwater.R;
import com.antoine.smartwater.entities.House;
import com.antoine.smartwater.entities.User;
import com.antoine.smartwater.utils.AuthListener;
import com.antoine.smartwater.utils.Constants;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class NewAccountActivity extends AppCompatActivity {

    private View adminView, nonAdminView;
    private String mEmail, mPassword;

    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private AuthListener mAuthListener;
    private DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_account);

        Intent intent = getIntent();
        mEmail = intent.getStringExtra(Constants.INTENT_EMAIL);
        mPassword = intent.getStringExtra(Constants.INTENT_PASSWORD);

        mAuthListener = new AuthListener();

        adminView = findViewById(R.id.layout_admin_mode);
        nonAdminView = findViewById(R.id.layout_non_admin_mode);

        SwitchCompat adminSwitch = (SwitchCompat) findViewById(R.id.admin_switch);
        adminSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    adminView.setVisibility(View.VISIBLE);
                    nonAdminView.setVisibility(View.GONE);
                } else {
                    adminView.setVisibility(View.GONE);
                    nonAdminView.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public void createNewAccount(View view) {
        EditText nameEditText = (EditText) findViewById(R.id.field_name);
        final String name = nameEditText.getText().toString();

        SwitchCompat adminSwitch = (SwitchCompat) findViewById(R.id.admin_switch);
        final boolean isAdmin = adminSwitch.isChecked();

        EditText housenameEditText = (EditText) findViewById(R.id.field_housename);
        final String houseName = housenameEditText.getText().toString();

        EditText houseIdEditText = (EditText) findViewById(R.id.field_house_id);
        final String houseId = houseIdEditText.getText().toString();

        Log.d(Constants.TAG, "createNewAccount: " + mEmail + " - " + mPassword);
        mAuth.createUserWithEmailAndPassword(mEmail, mPassword)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(Constants.TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Toast.makeText(getApplicationContext(), task.getException().toString(), Toast.LENGTH_SHORT).show();
                            return;
                        } else {
                            // Insert in DB
                            FirebaseUser firebaseUser = mAuth.getCurrentUser();

                            // User
                            User user = new User(name, firebaseUser, isAdmin);
                            if(isAdmin) {
                                // Create a new house
                                DatabaseReference houseRef = mDatabase.child("houses").push();
                                user.setHouse(houseRef.getKey());

                                House house = new House(houseName);
                                house.setAdmin(user);

                                // Save user in db (have to be done before saving house (security rules))
                                mDatabase.child("users").child(firebaseUser.getUid()).setValue(user);

                                // Save house in db
                                houseRef.setValue(house);
                            } else {
                                user.setHouse(houseId);
                                // Save user in db
                                mDatabase.child("users").child(firebaseUser.getUid()).setValue(user);
                            }

                            // Router activity will "auto-connect" the user
                            Intent intent = new Intent(NewAccountActivity.this, RouterActivity.class);
                            startActivity(intent);
                        }
                    }
                });
    }

    public void backToLogin(View view) {
        Intent intent = new Intent(NewAccountActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAuth.removeAuthStateListener(mAuthListener);
    }

}

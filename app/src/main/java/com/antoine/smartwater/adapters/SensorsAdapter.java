package com.antoine.smartwater.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.antoine.smartwater.R;
import com.antoine.smartwater.entities.Sensor;
import com.antoine.smartwater.utils.Constants;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by antoine on 23/05/17.
 */

public class SensorsAdapter extends ArrayAdapter<Sensor> {

    private ArrayList<Sensor> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView name;
        TextView room;
        ImageView type;
        TextView availability;
    }

    public SensorsAdapter(ArrayList<Sensor> data, Context context) {
        super(context, R.layout.sensor_row_layout, data);
        this.dataSet = data;
        this.mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the sensor item for this position
        Sensor sensor = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.sensor_row_layout, parent, false);
            viewHolder.name = (TextView) convertView.findViewById(R.id.sensor_name);
            viewHolder.room = (TextView) convertView.findViewById(R.id.sensor_room);
            viewHolder.availability = (TextView) convertView.findViewById(R.id.sensor_availability);
            viewHolder.type = (ImageView) convertView.findViewById(R.id.sensor_icon);

            result = convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        viewHolder.name.setText(sensor.getName());
        viewHolder.room.setText(sensor.getRoom());

        switch (sensor.getType()) {
            case TAP:
                viewHolder.type.setBackgroundResource(R.drawable.ic_tap_blac_48px);
                break;
            case SHOWER:
                viewHolder.type.setBackgroundResource(R.drawable.ic_shower_black_48px);
                break;
            case UNKNOWN:
                viewHolder.type.setBackgroundResource(R.drawable.ic_help_black_24dp);
                break;
        }

        viewHolder.availability.setText(" - Offline");
        if(sensor.isEnabled())
            viewHolder.availability.setText(" - Connected");


        // Return the completed view to render on screen
        return convertView;
    }

    @Override
    public boolean isEnabled(int position) {
        return dataSet.get(position).isEnabled();
    }
}
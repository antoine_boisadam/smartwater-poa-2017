package com.antoine.smartwater.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.antoine.smartwater.R;
import com.antoine.smartwater.entities.Recording;
import com.antoine.smartwater.entities.Sensor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by antoine on 23/05/17.
 */

public class RecordingAdapter extends ArrayAdapter<Recording> {

    private ArrayList<Recording> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView quantity;
        TextView date;
        ImageView icon;
    }

    public RecordingAdapter(ArrayList<Recording> data, Context context) {
        super(context, R.layout.history_row_layout, data);
        this.dataSet = data;
        this.mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the record item for this position
        Recording record = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.history_row_layout, parent, false);
            viewHolder.quantity = (TextView) convertView.findViewById(R.id.record_quantity);
            viewHolder.date = (TextView) convertView.findViewById(R.id.record_date);
            viewHolder.icon = (ImageView) convertView.findViewById(R.id.record_icon);

            result = convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyy");
        viewHolder.date.setText(dateFormat.format(record.getDate()));
        viewHolder.quantity.setText(record.getConsumption() + " " + getString(R.string.liters));

        switch (record.getSensorType()) {
            case TAP:
                viewHolder.icon.setBackgroundResource(R.drawable.ic_tap_blac_48px);
                break;
            case SHOWER:
                viewHolder.icon.setBackgroundResource(R.drawable.ic_shower_black_48px);
                break;
            case UNKNOWN:
                viewHolder.icon.setBackgroundResource(R.drawable.ic_help_black_24dp);
                break;
        }

        // Return the completed view to render on screen
        return convertView;
    }

    @NonNull
    private String getString(int resourceId) {
        return mContext.getResources().getString(resourceId);
    };
}
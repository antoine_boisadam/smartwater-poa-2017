# Scripts

Ce dossier contient les scipts utilisés pour générer des données.

### Config

Modifier les 4 constantes relatives au compte à modifier, au début du script `js`.

### Lancement

```
node fakedata-generator
```

### Sortie

Le script va afficher dans le terminal une suite d'enregistrement. Il faudrait sauvegarder cette sortie dans un fichier `.json`. Par exemple : `node fakedata-generator > data.json`.  
Puis importer ce fichier sur la base de donnée Firebase dans la zone : `/recordings/:houseId:/:userId:/`.  
:warning: L'import écrase les données déjà en ligne, il faut donc faire attention à la zone d'importation.

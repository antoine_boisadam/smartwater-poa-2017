'use strict';

// FICHIER PAS A JOUR AVEC LE DERNIER MODELE.

const moment = require('moment');

// Sensors
let sensors = [
  {
    "id" : 0,
    "mac" : "98:D3:37:00:AF:CE",
    "mode" : "BLUETOOTH",
    "name" : "Douche",
    "room" : "Bathroom",
    "type" : "SHOWER"
  },
  {
    "id" : 1,
    "mac" : "60:01:94:0e:0e:b8",
    "mode" : "WIFI",
    "name" : "Robinet",
    "room" : "Kitchen",
    "type" : "TAP"
  }
];

// Dates
let startDate = moment("15/05/2017", "DD/MM/YYYY");
let endDate = moment("26/06/2017", "DD/MM/YYYY");


// Generation
function random(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min +1)) + min;
}
let ONE_DAY = 1 * 24 * 60 * 60;
let records = [];

for(var i = startDate.unix()/ONE_DAY; i < endDate.unix()/ONE_DAY; i++) {
  records.push({
    "consumption": Math.random() * 40,
    "date" : (startDate.unix()+(i - startDate.unix()/ONE_DAY)*ONE_DAY+random(5*60*60, 10*60*60))*1000,
    sensor: sensors[random(0, sensors.length-1)]
  });
}

console.log(JSON.stringify(records));
return 0;

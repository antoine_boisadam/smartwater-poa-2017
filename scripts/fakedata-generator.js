'use strict';
const moment = require('moment');

// Sensors
let sensors = [
  {
    "id" : "-KoSCqo6CegwwkpYnW0Q",
    "mac" : "52:55:0a:00:02:02",
    "mode" : "WIFI",
    "name" : "Douche",
    "room" : "Bathroom",
    "type" : "SHOWER",
    "userid" : "DlVjjTDJqfWRWTSx8AOX6PXVvp23"
  },
  {
    "id" : "-Kowl2Jg11jWUaQ0rUa2",
    "mac" : "98:D3:37:00:AF:CE",
    "mode" : "BLUETOOTH",
    "name" : "Robinet cuisine",
    "room" : "Kitchen",
    "type" : "TAP",
    "userid" : "DlVjjTDJqfWRWTSx8AOX6PXVvp23"
  },
  {
    "id" : "-KpQqjFl6hsL81xX7fAY",
    "mac" : "ee:93:3d:79:d1:d1",
    "mode" : "WIFI",
    "name" : "Lavabo",
    "room" : "Bathroom",
    "type" : "TAP",
    "userid" : "DlVjjTDJqfWRWTSx8AOX6PXVvp23"
  }
];

// Dates
let startDate = moment("25/05/2017", "DD/MM/YYYY");
let endDate = moment("03/08/2017", "DD/MM/YYYY");

// User
let userId = "DlVjjTDJqfWRWTSx8AOX6PXVvp23";


// Script
function random(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min +1)) + min;
}
let ONE_DAY = 1 * 24 * 60 * 60;
let records = {};

// init
for(var j = 0; j < sensors.length; j++) {
  records[sensors[j].id] = [];
}

// generetion
var sensorId, temp;
for(var i = startDate.unix()/ONE_DAY; i < endDate.unix()/ONE_DAY; i++) {
  temp = random(0, sensors.length-1);
  sensorId = sensors[temp].id;
  records[sensorId].push({
    "consumption": Math.random() * 40,
    "date" : (startDate.unix()+(i - startDate.unix()/ONE_DAY)*ONE_DAY+random(5*60*60, 10*60*60))*1000,
    sensorType: sensors[temp].type,
    sensorid: sensorId,
    userid: userId
  });
}

console.log(JSON.stringify(records));
return 0;

# SMARTWATER #

Ce dépot contient les sources de l'application mobile Android développée dans le cadre de mon stage de fin de 4eme année.
J'ai effectué ce stage à l'Institut d'Informatique de l'UFRGS, à Porto Alegre au Brésil.

![Screenshots](screenshots.png)

> Antoine Boisadam - 2017
